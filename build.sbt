lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.nedellis",
      scalaVersion := "2.13.3"
    )),
    name := "sanvaad"
  )

libraryDependencies ++= Seq(
  "org.mindrot" % "jbcrypt" % "0.4",
  "com.lihaoyi" %% "cask" % "0.7.8",
  "org.xerial" % "sqlite-jdbc" % "3.34.0",
  "io.getquill" %% "quill-jdbc" % "3.6.0",
  "com.pauldijou" %% "jwt-upickle" % "5.0.0",
  "org.apache.commons" % "commons-text" % "1.9"
)

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % Test
