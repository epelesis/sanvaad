package com.nedellis.sanvaad.state

case class Conversation()

object Conversation extends SQLTable {
  override def createStmt: String =
    """CREATE TABLE IF NOT EXISTS CONVERSATION(
      |	id INTEGER PRIMARY KEY
      |);
      |""".trim.stripMargin

  implicit def rw: upickle.default.ReadWriter[Conversation] = upickle.default.macroRW[Conversation]

}

//case class User(id: Long, name: String, hashedPassword: String)
//
//object User extends SQLTable {
//  override def createStmt: String =
//    """CREATE TABLE IF NOT EXISTS USER(
//      |	id INTEGER PRIMARY KEY,
//      | name TEXT NOT NULL UNIQUE,
//      | hashedPassword TEXT NOT NULL
//      |);
//      |""".trim.stripMargin
//
//  implicit def rw: upickle.default.ReadWriter[User] = upickle.default.macroRW[User]
//
//  def createNewUser(name: String, plaintextPassword: String, db: DB): Long = {
//    import db.ctx._
//
//    val hashedPassword = hashPassword(plaintextPassword)
//
//    run(quote {
//      query[User].insert(lift(User(0L, name, hashedPassword))).returningGenerated(_.id)
//    })
//  }
//
//  def getUser(id: Long, db: DB): User = {
//    import db.ctx._
//
//    run(query[User].filter(_.id == lift(id))).head
//  }
//
//  def hashPassword(plaintext: String): String = BCrypt.hashpw(plaintext, BCrypt.gensalt)
//
//  def comparePassword(hashedCandidate: String, hashedPassword: String): Boolean = BCrypt.checkpw(hashedCandidate, hashedPassword)
//}
