package com.nedellis.sanvaad.state

import java.time.Instant

case class Message(id: Long, message: String, conversationId: Long, createdAt: Long)

object Message extends SQLTable {
  override def createStmt: String =
    """CREATE TABLE IF NOT EXISTS MESSAGE(
      |	id INTEGER PRIMARY KEY,
      | message TEXT NOT NULL,
      | conversationId INTEGER NOT NULL REFERENCES CONVERSATION(conversationId),
      | createdAt INTEGER NOT NULL
      |);
      |""".trim.stripMargin

  implicit def rw: upickle.default.ReadWriter[Message] = upickle.default.macroRW[Message]

  def createNewMessage(message: String, conversationId: Long, db: DB): Long = {
    import db.ctx._

    val createdAt = Instant.now().getEpochSecond

    run(quote {
      query[Message].insert(lift(Message(0L, message, conversationId, createdAt))).returningGenerated(_.id)
    })
  }

  def getMessage(id: Long, db: DB): Message = {
    import db.ctx._

    run(query[Message].filter(_.id == lift(id))).head
  }
}
