package com.nedellis.sanvaad.state

case class ConversationLink()

object ConversationLink extends SQLTable {
  override def createStmt: String =
    """CREATE TABLE IF NOT EXISTS CONVERSATION_LINK(
      |	id INTEGER PRIMARY KEY,
      | userId INTEGER NOT NULL REFERENCES USER(userId),
      | conversationId INTEGER NOT NULL REFERENCES CONVERSATION(conversationId)
      |);
      |""".trim.stripMargin
}
