package com.nedellis.sanvaad.state

import io.getquill.{LowerCase, SqliteJdbcContext}

trait SQLTable {
  def createStmt: String

  def createTable(db: DB): Unit = {
    assert(db.ctx.executeAction(createStmt) == 0L)
  }
}

class DB {
  lazy val ctx = new SqliteJdbcContext(LowerCase, "ctx")

  User.createTable(this)
  Conversation.createTable(this)
  ConversationLink.createTable(this)
  Message.createTable(this)
}
