package com.nedellis.sanvaad.auth

import com.nedellis.sanvaad.state.{DB, User}
import org.apache.commons.text.RandomStringGenerator
import pdi.jwt.{JwtAlgorithm, JwtClaim, JwtUpickle}

import java.security.SecureRandom
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap
import scala.util.{Failure, Success}

class Auth(db: DB) {

  import Auth._

  private val claimUserMapping = new ConcurrentHashMap[JwtClaim, User.UserId]

  private val randomEncryptionToken = generateRandomToken()

  def createUserClaim(id: User.UserId): String = {
    val claim = JwtClaim(
      expiration = Some(Instant.now.plusSeconds(60 * 10L).getEpochSecond),
      issuedAt = Some(Instant.now.getEpochSecond)
    )
    claimUserMapping.put(claim, id)
    JwtUpickle.encode(claim, randomEncryptionToken, JwtAlgorithm.HS256)
  }

  def verifyUserClaim(providedToken: String): Option[User.UserId] = {
    JwtUpickle.decode(providedToken, randomEncryptionToken, Seq(JwtAlgorithm.HS256)) match {
      case Success(claim) => Option(claimUserMapping.get(claim))
      case Failure(_) => None
    }
  }
}

object Auth {
  val TOKEN_LENGTH = 20

  def generateRandomToken(): String = {
    val random = new SecureRandom()
    val generator = new RandomStringGenerator.Builder()
      .usingRandom((max: Int) => random.nextInt(max))
      .build()
    generator.generate(TOKEN_LENGTH)
  }
}
