package com.nedellis.sanvaad

object Common {
  type ErrorMsg = String

  sealed trait Result[T]

  object Result {
    implicit def rw[T: upickle.default.ReadWriter]: upickle.default.ReadWriter[Result[T]] = upickle.default.macroRW[Result[T]]
  }

  case class Success[T](v: T) extends Result[T]

  object Success {
    implicit def rw[T: upickle.default.ReadWriter]: upickle.default.ReadWriter[Success[T]] = upickle.default.macroRW[Success[T]]
  }

  case class Failure[T](why: ErrorMsg) extends Result[T]

  object Failure {
    implicit def rw[T: upickle.default.ReadWriter]: upickle.default.ReadWriter[Failure[T]] = upickle.default.macroRW[Failure[T]]
  }

  def resultNotImplemented[T](): Result[T] = Failure("Not Implemented")
}
