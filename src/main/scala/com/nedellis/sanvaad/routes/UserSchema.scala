package com.nedellis.sanvaad.routes

import com.nedellis.sanvaad.Common._
import com.nedellis.sanvaad.state.User

object UserSchema {

  case class CreateUserReq(name: String, plaintextPassword: String)

  object CreateUserReq {
    implicit def rw: upickle.default.ReadWriter[CreateUserReq] = upickle.default.macroRW[CreateUserReq]
  }

  case class CreateUserRes(userId: Result[Long])

  object CreateUserRes {
    implicit def rw: upickle.default.ReadWriter[CreateUserRes] = upickle.default.macroRW[CreateUserRes]
  }

  case class AuthenticateUserReq(plaintextPassword: String)

  object AuthenticateUserReq {
    implicit def rw: upickle.default.ReadWriter[AuthenticateUserReq] = upickle.default.macroRW[AuthenticateUserReq]
  }

  case class AuthenticateUserRes(claimToken: Result[String])

  object AuthenticateUserRes {
    implicit def rw: upickle.default.ReadWriter[AuthenticateUserRes] = upickle.default.macroRW[AuthenticateUserRes]
  }


  //  case class GetUserRes(user: Result[User])
  //
  //  object GetUserRes {
  //    implicit def rw: upickle.default.ReadWriter[GetUserRes] = upickle.default.macroRW[GetUserRes]
  //  }

}
