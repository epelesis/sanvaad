package com.nedellis.sanvaad.routes

import com.nedellis.sanvaad.state.Message
import com.nedellis.sanvaad.Common._

object MessageSchema {

  case class CreateMessageReq(message: String, conversationId: Long)

  object CreateMessageReq {
    implicit def rw: upickle.default.ReadWriter[CreateMessageReq] = upickle.default.macroRW[CreateMessageReq]
  }

  case class CreateMessageRes(messageId: Long)

  object CreateMessageRes {
    implicit def rw: upickle.default.ReadWriter[CreateMessageRes] = upickle.default.macroRW[CreateMessageRes]
  }

  case class GetMessageRes(message: Result[Message])

  object GetMessageRes {
    implicit def rw: upickle.default.ReadWriter[GetMessageRes] = upickle.default.macroRW[GetMessageRes]
  }

}
