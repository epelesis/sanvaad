package com.nedellis.sanvaad.routes

import com.nedellis.sanvaad.state.{DB, Message}
import com.nedellis.sanvaad.Common._

case class MessageRoute(db: DB)(implicit log: cask.Logger) extends cask.Routes {

  import MessageSchema._

  @cask.postJson("/messages/create")
  def createUser(req: CreateMessageReq): CreateMessageRes = {
    val messageId = Message.createNewMessage(req.message, req.conversationId, db)
    CreateMessageRes(messageId)
  }

  @cask.getJson("/messages/message/:id")
  def getUser(id: String): GetMessageRes = {
    GetMessageRes(Success(Message.getMessage(id.toLong, db)))
  }

  initialize()
}
