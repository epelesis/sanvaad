package com.nedellis.sanvaad.routes

import com.nedellis.sanvaad.Common._
import com.nedellis.sanvaad.auth.Auth
import com.nedellis.sanvaad.state.{DB, User}

case class UserRoute(db: DB, auth: Auth)(implicit log: cask.Logger) extends cask.Routes {

  import UserSchema._

  @cask.postJson("/users/create")
  def createUser(req: CreateUserReq): CreateUserRes = {
    val userId = User.createNewUser(req.name, req.plaintextPassword, db)

    CreateUserRes(Success(userId))
  }

  @cask.postJson("/users/user/:id/authenticate")
  def authenticateUser(id: String, req: AuthenticateUserReq): AuthenticateUserRes = {
    val requestedUser = User.getUser(id.toLong, db)

    if (User.comparePassword(req.plaintextPassword, requestedUser.hashedPassword)) {
      val claim = auth.createUserClaim(requestedUser.id)
      AuthenticateUserRes(Success(claim))
    } else {
      AuthenticateUserRes(Failure("Invalid user ID + password combination"))
    }
  }

  initialize()
}
