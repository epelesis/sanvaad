package com.nedellis.sanvaad

import com.nedellis.sanvaad.auth.Auth
import com.nedellis.sanvaad.routes.{ConversationRoute, MessageRoute, UserRoute}
import com.nedellis.sanvaad.state.DB

object Main extends cask.Main {
  val db = new DB()
  val auth = new Auth(db)
  val allRoutes = Seq(UserRoute(db, auth), ConversationRoute(db), MessageRoute(db))
}
